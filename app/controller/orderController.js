const { default: mongoose } = require('mongoose');

const userModel = require("../models/userModel");
const orderModel = require("../models/orderModel");

const createOrder = (req, res) => {
    var userId = req.params.userId;

    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            message: `Error 400: User Id khong dung dinh dang.`
        });
    };

    var body = req.body;

    if (!body.pizzaSize) {
        return res.status(400).json({
            message: `Error 400: Pizza Size phai bat buoc.`
        });
    };

    if (!body.pizzaType) {
        return res.status(400).json({
            message: `Error 400: Pizza Type phai bat buoc.`
        });
    };

    if (!body.status) {
        return res.status(400).json({
            message: `Error 400: Trang thai phai bat buoc.`
        });
    };

    const newOrder = new orderModel({
        _id: mongoose.Types.ObjectId(),

        pizzaSize: body.pizzaSize,

        pizzaType: body.pizzaType,

        voucher: body.voucher,

        drink: body.drink,

        status: body.status,
    });

    orderModel.create(newOrder, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Error 500: ${error.message}`
            });
        } else {
            userModel.findByIdAndUpdate(userId,
                {
                    $push: { order: data._id }
                },
                (err) => {
                    if (err) {
                        return res.status(500).json({
                            message: `Error 500: ${err.message}`
                        });
                    } else {
                        return res.status(201).json({
                            message: `Tao moi Order thanh cong.`,
                            order: data
                        })
                    }
                }
            )
        }
    })
};

const getAllOrder = (req, res) => {
    var userId = req.params.userId;

    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            message: `Error 400: User Id khong dung dinh dang.`
        });
    };

    userModel.findById(userId)
        .populate("order")
        .exec((err, data) => {
            if (err) {
                return res.status(500).json({
                    message: `Error 500: ${err.message}`
                });
            } else {
                return res.status(200).json({
                    message: `Lay du lieu Order thanh cong.`,
                    order: data.order
                })
            }
        })
};

const getOrderById = (req, res) => {
    var orderId = req.params.orderId;

    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            message: `Error 400: Order Id khong dung dinh dang.`
        });
    };

    orderModel.findById(orderId, (err, data) => {
        if (err) {
            return res.status(500).json({
                message: `Error 500: ${err.message}`
            });
        } else {
            return res.status(200).json({
                message: `Lay du lieu Order thanh cong.`,
                order: data
            })
        }
    })
};

const updateOrderById = (req, res) => {
    var orderId = req.params.orderId;

    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            message: `Error 400: Order Id khong dung dinh dang.`
        });
    };

    var body = req.body;

    if (!body.pizzaSize) {
        return res.status(400).json({
            message: `Error 400: Pizza Size phai bat buoc.`
        });
    };

    if (!body.pizzaType) {
        return res.status(400).json({
            message: `Error 400: Pizza Type phai bat buoc.`
        });
    };

    if (!body.status) {
        return res.status(400).json({
            message: `Error 400: Trang thai phai bat buoc.`
        });
    };

    const order = new orderModel({
        pizzaSize: body.pizzaSize,

        pizzaType: body.pizzaType,

        voucher: body.voucher,

        drink: body.drink,

        status: body.status,
    });

    orderModel.findByIdAndUpdate(orderId, order, (err, data) => {
        if (err) {
            return res.status(500).json({
                message: `Error 500: ${err.message}`
            });
        } else {
            return res.status(200).json({
                message: `Cap nhat du lieu Order thanh cong.`,
                order: data
            })
        }
    })
};

const deleteOrderById = (req, res) => {
    var userId = req.params.userId;

    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            message: `Error 400: User Id khong dung dinh dang.`
        });
    };

    var orderId = req.params.orderId;

    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            message: `Error 400: Order Id khong dung dinh dang.`
        });
    };

    orderModel.findByIdAndDelete(orderId, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Error 500: ${error.message}`
            });
        } else {
            userModel.findByIdAndUpdate(userId,
                {
                    $pull: { order: orderId }
                },
                (err) => {
                    if (err) {
                        return res.status(500).json({
                            message: `Error 500: ${err.message}`
                        });
                    } else {
                        return res.status(204).json({
                            message: `Xoa Order thanh cong.`,
                            order: data
                        })
                    }
                }
            )
        }
    })
};

module.exports = {
    createOrder,
    getAllOrder,
    getOrderById,
    updateOrderById,
    deleteOrderById
}
